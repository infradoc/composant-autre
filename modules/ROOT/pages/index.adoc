= Autre
:lang:fr

La catégorie `autre` permet d'y mettre des articles qui ne peuvent pas rentrer dans une des trois autres catégories comme certains projets particuliers.
